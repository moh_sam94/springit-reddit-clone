# Springit Reddit Clone

Minimal [Spring Boot](http://projects.spring.io/spring-boot/) sample app.

## Overview

This project was implemented from [Udemy Spring Boot Course](https://www.udemy.com/share/1027TCB0obcFdWR3Q=/)
It is an app where the user can enter his tasks then the app decides randomly what to todo next.

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `de.codecentric.springbootsample.Application` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

## Package
```shell
mvn clean package
```

## Run JAR
```shell
java -jar target/springit-0.0.1-SNAPSHOT.jar
```

# Local Mail Server
 - install [Maildev](https://maildev.github.io/maildev/) then run it.

## Copyright

Released under the Apache License 2.0. See the [LICENSE](https://github.com/codecentric/springboot-sample-app/blob/master/LICENSE) file.